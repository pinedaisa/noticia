<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	protected $table = 'comments';
	protected $fillable = [
        'name','text', 'notice_id'
    ];
    function notice(){
   	return $this->belongsTo ('App\Notice');
   }
}
