<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Comment;

class CommentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     *
     */
      /**
     * Show the application dashboard.
     *
    
     */
    public function addcomment(Request $request)
    {
        $this->validate($request,[
            'nombres' => 'required',
            'comment' => 'required',
            ]);
        $name= $request->nombres;
        $comment = new Comment;
        $comment->name = $request->nombres;
        $comment->text = $request->comment;
        $comment->notices_id = $request->noticeid;
        $comment->save();
        return json_encode( $comment);
       
    }

    
}
