<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Author;
use App\Notice;
use App\Comment;
use Storage;

class News extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return view('news.news');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'description' => 'required',
            'urlImg' => 'image|required',
            ]);
        $news = new Notice;
        $news->title = $request->title;
        $news->description = $request->description;
        $news->authors_id= $request->authors;
        $img= $request->file('urlImg');
        $file_route=time().'_'.$img->getClientOriginalName();
        //Con esta instrucción guardamos la imagen en el Disco que se creo en config/File System
        Storage::disk('imgNoticias')->put($file_route, file_get_contents( $img->getRealPath() ) );
        $news->urlImg=$file_route;
        if ($news->save()){
            return back()->with('msj','Datos Guardados');
        }else{
            return back()->with('errormsj','Los datos no se guardaron');;
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notice = Notice::find($id);
        return view('news.edit')->with(['edit'=>'true','notice'=>$notice]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required',
            'description' => 'required',
            'urlImg' => 'image|required',
            ]);
        $news = Notice::find($id);
        $news->title = $request->title;
        $news->description = $request->description;
        $news->authors_id= $request->authors;
        $img= $request->file('urlImg');
        $file_route=time().'_'.$img->getClientOriginalName();
        //Con esta instrucción guardamos la imagen en el Disco que se creo en config/File System
        Storage::disk('imgNoticias')->put($file_route, file_get_contents( $img->getRealPath() ) );
        Storage::disk('imgNoticias')->delete($request->img);
        $news->urlImg=$file_route;
        if ($news->save()){
            $notice= Notice::all();
            return view('news.list')->with(['notice' => $notice]);
        }else{
            return back()->with('errormsj','Los datos no se guardaron');;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Notice::destroy($id);
       return back();
    }

    //Mostrar las noticias
    public function mostrar()
    {
       $notice=Notice::all();
       $comment= Comment::all();
       $comment3=Comment::select('notices_id', 'name', 'text')->groupBy('notices_id','name','text')->take(6)->get();
      
       return view('welcome')->with(['notice' => $notice , 'comment' => $comment, 'comment3' => $comment3]);
    }
    //Fultra los autores ya registrados
    public function authors(Request $request){
        $term = $request->input('term') ?: '';
        $author = Author::where('name', 'like', '%'.$term.'%')->pluck('name', 'id');
        $data = [];
        foreach ($author as $id => $name) {
          $data[] = ['id' => $id, 'text' => $name];
        }
        return json_encode($data);
    }

    //list news
    public function listanoticia()
    {
        $notice= Notice::all();
        return view('news.list')->with(['notice' => $notice]);
    }
}
