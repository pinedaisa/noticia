<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{ 
	protected $table = 'authors';
	protected $fillable = [
        'name','apellido', 'email','phone'
    ];
   function notice(){
   	return $this->belongsTo('App\Notice'); 
   }
   function phones(){
   	return $this->hasMany('App\Phone'); 
   }
}
