<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
    protected $table = 'notices';
    protected $fillable = [
        'title', 'description','authors_id', 'urlImg'
    ];
     function authors(){
   	return $this->belongsTo('App\Author');
   }
}
