<div class="container">

        <hr>

        <div class="row">
            <div class="col-sm-8">
                <h2>Sistema de Noticias</h2>
                <p>Este sistema permite el registro de las noticias , asi como la información perteneciente al autor de las mismas y también con la posibilidad de que los lectores puedan poner comentarios  </p>
                <p>
                    <a class="btn btn-default btn-lg" href="#">Call to Action &raquo;</a>
                </p>
            </div>
            <div class="col-sm-4">
                <h2>Contactenos</h2>
                <address>
                    <strong>Isabel Pineda</strong>
                    <br>Medellín-Colombia
                    <br>
                </address>
                <address>
                    <abbr title="Phone">Tlf:</abbr>(312 8507216)
                    <br>
                    <abbr title="Email">E:</abbr> <a href="mailto:#">pinedaisa@gmail.com</a>
                </address>
            </div>
        </div>
        <!-- /.row -->

        <hr>

        <div class="row">
            <div class="col-sm-4">
                <img class="img-circle img-responsive img-center" src="/img/autor.jpg" alt="">
                <h2>Autor</h2>
                <p>En este modúlo podras registrar los diferente autores de la noticias</p>
            </div>
            <div class="col-sm-4">
                <img class="img-circle img-responsive img-center" src="/img/noticias.jpg" alt="">
                <h2>Noticias</h2>
                <p>En este modúlo podras registrar las noticias con su respectiva foto.</p>
            </div>
            <div class="col-sm-4">
                <img class="img-circle img-responsive img-center" src="/img/comentario.jpg" alt="">
                <h2>Comentarios</h2>
                <p>Permite realizar comentarios</p>
            </div>
        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2017</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>