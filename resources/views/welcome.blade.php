<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Sistema') }}</title>

    <!-- Styles -->
    @yield('css')
    <link href="/css/app.css" rel="stylesheet"/>
  
   <link href="/css/styles.css" rel="stylesheet"/>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    
    <!-- core CSS -->
    <link href="css/blog/bootstrap.min.css" rel="stylesheet">
    <link href="css/blog/font-awesome.min.css" rel="stylesheet">
    <link href="css/blog/prettyPhoto.css" rel="stylesheet">
    <link href="css/blog/animate.min.css" rel="stylesheet">
    <link href="css/blog/main.css" rel="stylesheet">
    <link href="css/blog/responsive.css" rel="stylesheet">
    
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head>
<body>
 <header id="header">

        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img src="images/logo.jpg" alt="logo"></a>
                </div>
                 <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li></li>
                             <li></li>
                            <li></li>
                           
                        @else

                           
                        @endif
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
        
    </header><!--/header-->
   

    @yield('content')
     <section id="blog" class="container">
        <div class="center">
            <h2>Blogs Informátivo</h2>
            <p class="lead">Nuevas Técnologias Web</p>
        </div>

        <div class="blog">
            <div class="row">
                 <div class="col-md-8">
                    <div class="blog-item">
                        <div class="row">
                            <div class="col-xs-12 col-sm-2 text-center">
                                <div class="entry-meta">
                                    <span id="publish_date">07  NOV</span>
                                    <span><i class="fa fa-user"></i> <a href="#">John Doe</a></span>
                                    <span><i class="fa fa-comment"></i> <a href="blog-item.html#comments">2 Comments</a></span>
                                    <span><i class="fa fa-heart"></i><a href="#">56 Likes</a></span>
                                </div>
                            </div>
                                
                            <div class="col-xs-12 col-sm-10 blog-content ">
                                @if(isset($notice))
                                    @foreach($notice as $n)
                                      
                                               
                                                <img src="/imgNoticias/{{ $n->urlImg }}" class="img-responsive"  width="100%"  alt="Responsive image">

                                                    <h2><a >{{$n->title}}</a></h2>
                                              
                                                <div class="panel-body" >
                                                    <p><div>{!! $n->description !!}</div></p>
                                                </div>
                                            
                                            <div class="panel-footer"> Autor de la Noticia:  {{$n->authors->name}}</div>
                   
                                            <div class="comment">
                                                <h3>Comentarios:</h3>
                                                @if(isset($comment))
                                                    @foreach($comment as $c)
                                                        @if($c->notices_id==$n->id)
                                                           
                                                            <div class="comment-avatar">
                                                                <img width="48" height="48" src="/imgNoticias/user.png" />    
                                                            </div>

                                                            <div class="comment-autor">
                                                                <strong> {{ $c->name }}</strong> dice:<br/>
                                                                <small> {{ $c->created_at }}</small>
                                                            </div>
                                                            <div class="comment-text">{{ $c->text }}</div>
                                                       @endif
                                                    @endforeach
                                                @endif
                                                <div  id="{{ 'newmessage'.$n->id }}"></div>
                                                <h2>Deja tu comentario </h2>
                                                 <input type="text"   id="{{ 'nombres'.$n->id }}" size="40"  required/><br/><br/>
                                                Comentario:<br/>
                                                <textarea   rows="6" cols="65"  id="{{ 'comment'.$n->id }}"></textarea>
                                                <input  class="hide" type="text" id="{{ 'noticeid'.$n->id }}" value=" {{ $n->id}} ">
                                                <br />
                                                <input type="submit"  value="Comment"  id="{{ $n->id}}" class="send_comment"/>
                                                <br/><br/>
                                            </div>
                                       
                                    @endforeach
                                @endif
                            </div>
                        </div>    
                    </div><!--/.blog-item-->
                </div>
                 <aside class="col-md-4">
                    <div class="widget search">
                        <form role="form">
                                <input type="text" class="form-control search_box" autocomplete="off" placeholder="Search Here">
                        </form>
                    </div><!--/.search-->
                    
                    <div class="widget categories">
                        <h3>Recent Comments</h3>
                        <div class="row">
                            <div class="col-sm-12">
                                @if(isset($notice))
                                    @foreach($notice as $n)
                                        <div class="panel-heading">
                                            <h2><a >{{$n->title}}</a></h2>
                                        </div>
                                        @if(isset($comment3))
                                            @foreach($comment3 as $c)
                                                @if($c->notices_id==$n->id)
                                                 <div class="single_comments">
                                                    <img width="48" height="48" src="/imgNoticias/user.png" />    
                                                    <p>{{ $c->text }}</p>
                                                    <div class="entry-meta small muted">
                                                        <span>By <a href="#"> {{ $c->name }}</span <span>On <a href="#">{{ $c->created_at }}</a></span>
                                                    </div>
                                                    </div> 
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif        
                            </div>
                        </div>                     
                    </div><!--/.recent comments-->
                     
            </div>
        </div>

    



    
    <!-- Scripts -->
 <script src="/js/jquery.min.js"></script>
<script src="/js/app.js"></script>

<script src="/vendors/ckeditor/ckeditor.js"></script>
<script src="/js/news/welcome.js"></script>

    @yield('js')
      
</body>
</html>
