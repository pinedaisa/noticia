<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name='viewport' content='width=device-width, initial-scale=1'>    
    <title>Costos - @yield('title')</title>
    @yield('css')
     <link rel="stylesheet" href="{{URL::asset('css/bootstrap.css')}}" />
    <link rel="stylesheet" href="{{URL::asset('css/main.css')}}" />
    <link rel="stylesheet" href="{{URL::asset('css/jquery.sidr.dark.min.css')}}" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet"/><!-- font awesome -->
  </head>
   <body>
    @section('navbar')
    <header class="navbar">
      
      <section class="navbar-section">
       
          <span>Holaa!</span>
          <br>
           <span></span>
       
      </section>
    </header>
    @show
    <div class="container">
      @yield('content')
    </div>
    <div class="footer">
      <div class="chip">
       
      </div>
      <p><strong>v.0.1.45</strong></p>
    </div>
    @yield('js')
    <script src="{{URL::asset('js/jquery.min.js')}}"></script>
    <script src="{{URL::asset('js/main.js')}}"></script>
    <script src="{{URL::asset('js/jquery.sidr.min.js')}}"></script>
  </body>
</html>