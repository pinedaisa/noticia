@extends('layouts.app')

@section('content')

@section('css')
<link href="/css/main.css" rel="stylesheet">
@endsection
<div class="container">
    <div class="row">
    	<div class="col-md-8 col-md-offset-2">
	        <div class="panel-body">
	           	<form  role='form' method="POST" action={{url('authors')}}>
	           	{{ csrf_field() }}
				  <h1>Registro del Author</h1>
				  <div class="inset">
				  <p>
				    <label for="name">Nombre</label>
				   <input id="name" type="text" class="form-control" name="name" value="{{ old('name')}}">
	                    @if ($errors->has('name'))
	                        <span style="color:red;">
	                            <strong>{{ $errors->first('name') }}</strong>
	                        </span>
	                    @endif
				  </p>
				  <p>
				    <label for="apellido">Apellido</label>
				    <input id="apellido" type="text" class="form-control" name="apellido" value="{{ old('apellido') }}">
	                    @if ($errors->has('apellido'))
	                        <span style="color:red;">
	                            <strong>{{ $errors->first('apellido') }}</strong>
	                        </span>
	                    @endif
				  </p>
				   <p >
				    <label for="email" >Email</label>
				    <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" >
						@if ($errors->has('email'))
	                         <span style="color:red;">
	                            <strong>{{ $errors->first('email') }}</strong>
	                        </span>
	                    @endif
				  </p>
				   <p>
				    <label for="tel">Telefono Celular (3128507216)</label>
				    <input id="phone" type="text" type="text"  class="form-control" name="phone" value="{{ old('phone') }}" >

	                    @if ($errors->has('phone'))
	                         <span style="color:red;">
	                            <strong>{{ $errors->first('phone') }}</strong>
	                        </span>
	                    @endif
				  </p>
				  </div>
				  <p class="p-container">
				    
				    <input type="submit" name="go" id="go" value="Log in">
				  </p>
				</form>
	        </div>
    	</div>
	</div>
</div>
@endsection
@section('js')
	

@endsection

