@extends('layout.master ')
@section('content')
<form  method="POST">
  {{ csrf_field() }}
  <div class="columns">
    <div class="column col-6 centered">
      <h4><small class="label">Datos del Proveedor</small></h4>
      <div class="toast toast-danger hide" id="toast-error">
        <a class="btn btn-clear float-right"></a>
        <span class="icon icon-error_outline"></span>
        <span id="error_message">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
      </div>
      <br>
      <div class="form-group">
      <div class="row">
        <div class="large-12 columns">
        <label>Idioma
          <select name="pais">
            <option value="es" @if ($pais== 'es') selected @endif>España</option>
            <option value="co" @if ($pais== 'co') selected @endif>Colombia</option>
            <option value="ve" @if ($pais== 've') selected @endif>Venezuela</option>
          </select>
        </label>
        </div>
      </div>
      </div>
      <div class="form-group">
      <div class="row">
        <div class="large-6 columns">
        <label>Desea registrarse?
           <input class="form-input "  type="checkbox" name="registro" id="registro" value='si' @if ($registro) checked @endif /><label for="checkbox1">Si</label>
        </label>
        </div>
      </div>
      </div>
      <div class="form-group">
        <label class="form-label" for="nit">Nit</label>
        <input class="form-input "  type="text" name="nit" id="nit" value="{{ $nit }}" placeholder="G-1112222-2" required />
      </div>
      <div class="form-group">
        <label class="form-label" for="nombre">Nombre</label>
        <input class="form-input " value="{{ isset($proveedor) ? $proveedor->nombre : '' }}" type="text" name="nombre" id="nombre" placeholder="Luis Pernalete" required />
      </div>
      <button class="btn btn-primary btn-save" type="submit"><i class="fa fa-floppy-o"></i> Guardar</button>
      <button class="btn btn-cancel" onclick="window.history.back();">Cancelar</button>    
    </div>
  </div>
</form>
@stop