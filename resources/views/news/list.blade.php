@extends('layouts.app')

@section('content')

@section('css')
<link rel="stylesheet" href="/css/stacktable.css" />

@endsection
<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">Panel heading</div>
  <div class="col-md-8 col-md-offset-2">
  <!-- Table -->
  <table id='listnew' class='table stacktable' >
  	@if(isset($notice))
    <thead>
		<th>Titulo</th>
		<th>Descripción</th>
		<th>Autor</th>
		<th>Imagen</th>
		<th></th>
	</thead>
	<tbody>
	@foreach ($notice as $n)
		<tr>
			<td>{{ $n->title }}</td>
			<td><div>{!! $n->description !!}</div></td>
			<td>{{ $n->authors->name }}</td>
			<td><img src="/imgNoticias/{{ $n->urlImg }}" class="img-responsive"  width="100" height="100" alt="Responsive image"></td>
			<td>
				<a href="/news/{{ $n->id }}/edit" class="btn btn-warning btn-xs">Modificar</a>

				<form action="{{route('news.destroy',$n->id)}}" method="POST">
					<input type="hidden" name="_method" value="DELETE">
	           		{{ csrf_field() }}
	           		<input type='submit' class="btn btn-danger btn-xs" value="Eliminar"></input>
				</form>
				
			</td>
		</tr>
	@endforeach	
	</tbody>
	@endif	
		
  </table>
  </div>
</div>
</div>
@endsection
@section('js')

 	<script src="/js/news/stacktable.js"></script>
 	<script src="/js/news/list.js"></script>
@endsection