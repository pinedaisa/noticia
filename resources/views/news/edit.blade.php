@extends('layouts.app')

@section('content')
@section('css')
<link href="/css/main.css" rel="stylesheet">
<link rel="stylesheet" href="/css/select2.min.css" />
@endsection
@if(isset($notice))
<div class="container">
    <div class="row">
    	<div class="col-md-8 col-md-offset-2">
	        <div class="panel-body">
	           	<form  role='form' method="POST" enctype="multipart/form-data" action= "{{ route('news.update', $notice->id)}} ">
	           	<input type="hidden" name="_method" value="PUT">
	           	<input  class="hide" type="text" name="img" value=" {{ '$notice->urlImg'}} ">
	           	{{ csrf_field() }}
				  	<h1>Registro de la Noticia</h1>
				  	<div class="inset">
				   	@if(session()->has('msj'))
	        			<div class="alert alert-success" role='alert'>{{ session('msj') }}</div>
			       	@endif
			        @if(session()->has('errormsj'))
	        			<div class="alert alert-success" role='alert'>Error al guardar los datos</div>
			       	@endif
				  	<p>
				    	<label for="title">Titulo</label>
				   		<input id="title" type="text" class="form-control" name="title" value="{{ $notice->title }}">
	                    @if ($errors->has('title'))
	                        <span style="color:red;">
	                            <strong>{{ $errors->first('title') }}</strong>
	                        </span>
	                    @endif
				  	</p>
				  	<p>
				    	<label for="description">Descripción</label>
				    	<textarea class="ckeditor" id="description" type="textarea" class="form-control" rows="6" cols="70" name="description" value="{{ old('description') }}">
	                    </textarea>
	                    @if ($errors->has('description'))
	                        <span style="color:red;">
	                            <strong>{{ $errors->first('description') }}</strong>
	                        </span>
	                    @endif
				  	</p>
				  	<p>
				  		<label class="form-label" for="authors">Autor de la Noticia</label>
          				<select name="authors" id="authors" class="form-control" ><option value="{{ $notice->authors_id }}"> {{ $notice->authors->name }}</option>
          				</select>
        				@if ($errors->has('authors'))
                         <span style="color:red;">
                            <strong>{{ $errors->first('authors') }}</strong>
                        	</span>
                    	@endif     
				  	</p>
				  	<p >
				    	<label for="urlImg" >Imagen</label>
				    	<input type=file size=60 name="urlImg" class="form-control"  id="urlImg"><br><br>
						@if ($errors->has('urlImg'))
	                         <span style="color:red;">
	                            <strong>{{ $errors->first('urlImg') }}</strong>
	                        </span>
	                    @endif
				  	</p>
				  	
				  
				  </div>
				  <p class="p-container">
				    
				    <button type="submit" class="btn btn-warning" >Modificar</button>
				  </p>
				</form>
	        </div>
    	</div>
	</div>
</div>
@endif
@endsection
@section('js')

	<script src="/js/select2.min.js"></script>	
 	<script src="/js/news/news.js"></script>

@endsection