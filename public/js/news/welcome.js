$(document).ready(function() {
    
   
        $('.send_comment').click(function() {
        var id = $(this).attr("id");
        var comment= $("#comment"+id).val();
        var nombres = $("#nombres"+id).val();
        var noticeid= $("#noticeid"+id).val();
        //var _token= $("input#_token").val();
        var now = new Date();
        var date_show = now.getDate() + '-' + now.getMonth() + '-' + now.getFullYear() + ' ' + now.getHours() + ':' + + now.getMinutes() + ':' + + now.getSeconds();
        data={
            nombres : $("#nombres"+id).val(),
            comment : $("#comment"+id).val(),
            noticeid: $("#noticeid"+id).val(),
        } ;
           
        $.ajax({
            type: "POST",
            url: "/add/comment",
            data:data,
             cache: false,
            success: function(data) {
                $("#newmessage"+id).append('<div><div><img width="48" height="48" src="/imgNoticias/user.png" /></div><div><strong>'+nombres+'</strong> dice:<br/><small>'+date_show+'</small></div><div>'+comment+'</div></div>');
                $("#comment"+id).html("value", "").focus();
                $("#nombres"+id).html("value", "");
                $("#noticeid"+id).html("value", "");
              
            }
        });
        return false;
    });
});   