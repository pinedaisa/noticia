$(document).ready(function() {

  //Cargar select el authors
  $('#authors').select2({
    placeholder: "Seleccione el Autor de la Noticia",
    language: "es",
    minimumInputLength: 1,
    //minimumResultsForSearch: Infinity,
    escapeMarkup: function (markup) {
        return markup;
    },    
    ajax: {
      dataType: 'json',
      url: '/api/news',
      delay: 250,
      data: function(params) {
        return {
          term: params.term
        }
      },
      processResults: function (data, page) {
        return {
          results: data
        };
      },
    }    
  });
});