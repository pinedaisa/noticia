<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/*Route::match(['get','post'],'/',function(){
	$nit = Input::get('email');
	$name = Input::get('name');
	$pais = Input::get('pais');
	$registro = Input::get('registro');
 return view('proveedor',['nit'=>$nit,'pais'=>$pais,'name'=>$name, 'registro'=>$registro]);
});*/

Auth::routes();
Route::get('/', 'News@mostrar');

Route::get('/home', 'HomeController@index');

Route::get('/author','Authors@index');

//Adquirir todos los metodos que estan en el controlador Authors y convertirlos en Ruta.
Route::resource('/authors', 'Authors');

//Adquirir todos los metodos que estan en el controlador News y convertirlos en Ruta.
Route::resource('/news', 'News');

//Obetener el autor de en la vista de Noticias

Route::get('/api/news','News@authors');

//listado de las noticias
Route::get('/api/list','News@listanoticia');

//Add comment
Route::post('/add/comment','CommentController@addcomment');

